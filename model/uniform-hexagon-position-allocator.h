/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#ifndef UNIFORM_HEXAGON_POSITION_ALLOCATOR_H
#define UNIFORM_HEXAGON_POSITION_ALLOCATOR_H

#include "ns3/position-allocator.h"

namespace ns3 {

/**
 * \ingroup mobility
 * \brief Allocate the positions uniformely (with constant density) randomly within a regular hexagon.
 *
 * UniformHexagonPositionAllocator allocates the positions randomly within a hexagon \f$ H \f$ lying on the
 * plane \f$ z=0 \f$ having center at coordinates \f$ (x,y,0) \f$,
 * circumradius \f$ \rho \f$ and orientation  \f$ \theta \f$
 * The random positions are chosen such that,
 * for any subset \f$ S \subset H \f$, the expected value of the
 * fraction of points which fall into \f$ S \subset H \f$ corresponds
 * to \f$ \frac{|S|}{|H|} \f$, i.e., to the ratio of the area of the
 * subset to the area of the whole hexagon.
 */
class UniformHexagonPositionAllocator : public PositionAllocator
{
public:

  /**
   * Register this type with the TypeId system.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  UniformHexagonPositionAllocator ();
  virtual ~UniformHexagonPositionAllocator ();


  /**
   * generate gnuplot commands to print the hexagon and write them to file
   *
   * \param filename file to write to
   */
  void PrintToGnuplotFile (std::string filename);


  virtual Vector GetNext (void) const;
  virtual int64_t AssignStreams (int64_t stream);

private:

  /**
   *
   * \param p coordinates of a point q
   *
   * \return true if point q is inside a similar hexagon centered at
   * (0,0) and oriented with sides parallel to the y axis; false otherwise
   */
  bool IsInsideCenteredNoRotation (Vector q) const;

  Ptr<UniformRandomVariable> m_rv;  //!< pointer to uniform random variable
  double m_rho; //!< value of the circumradius of the hexagon
  double m_theta; //!< orientation of the hexagon
  double m_x;  //!< x coordinate of center of hexagon
  double m_y;  //!< y coordinate of center of hexagon
};


} // namespace ns3

#endif /* UNIFORM_HEXAGON_POSITION_ALLOCATOR_H */
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#include "uniform-hexagon-position-allocator.h"
#include "ns3/mobility-model.h"
#include "ns3/double.h"
#include "ns3/uinteger.h"
#include "ns3/log.h"
#include <cmath>
#include <fstream>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("UniformHexagonPositionAllocator");

NS_OBJECT_ENSURE_REGISTERED (UniformHexagonPositionAllocator);

TypeId
UniformHexagonPositionAllocator::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::UniformHexagonPositionAllocator")
    .SetParent<PositionAllocator> ()
    .SetGroupName ("Mobility")
    .AddConstructor<UniformHexagonPositionAllocator> ()
    .AddAttribute ("rho",
                   "The radius of the hexagon",
                   DoubleValue (1.0),
                   MakeDoubleAccessor (&UniformHexagonPositionAllocator::m_rho),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("theta",
                   "The orientation angle of the hexagon, in radians, counterclockwise from the positive y-axis",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&UniformHexagonPositionAllocator::m_theta),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("X",
                   "The x coordinate of the center of the  hexagon.",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&UniformHexagonPositionAllocator::m_x),
                   MakeDoubleChecker<double> ())
    .AddAttribute ("Y",
                   "The y coordinate of the center of the  hexagon.",
                   DoubleValue (0.0),
                   MakeDoubleAccessor (&UniformHexagonPositionAllocator::m_y),
                   MakeDoubleChecker<double> ())
  ;
  return tid;
}

UniformHexagonPositionAllocator::UniformHexagonPositionAllocator ()
{
  m_rv = CreateObject<UniformRandomVariable> ();
}

UniformHexagonPositionAllocator::~UniformHexagonPositionAllocator ()
{
}


void
UniformHexagonPositionAllocator::PrintToGnuplotFile (std::string filename)
{
  std::ofstream outFile;
  outFile.open (filename.c_str (), std::ios_base::out | std::ios_base::trunc);
  if (!outFile.is_open ())
    {
      NS_LOG_ERROR ("Can't open file " << filename);
      return;
    }

  outFile << "set object 1 polygon from \\\n";

  for (uint32_t vertexId = 0; vertexId < 6; ++vertexId)
    {
      // angle of the vertex w.r.t. y-axis
      double a = vertexId * (M_PI/3.0) + m_theta;
      double x =  - m_rho * sin (a) + m_x;
      double y =  m_rho * cos (a) + m_y;
      outFile << x << ", " << y << " to \\\n";
    }
  // repeat vertex 0 to close polygon
  uint32_t vertexId = 0;
  double a = vertexId * (M_PI/3.0) + m_theta;
  double x =  - m_rho * sin (a) + m_x;
  double y =  m_rho * cos (a) + m_y;
  outFile << x << ", " << y << std::endl;
}


Vector
UniformHexagonPositionAllocator::GetNext (void) const
{
  NS_LOG_FUNCTION (this);

  Vector p;
  do
    {
      p.x = m_rv->GetValue (-m_rho, m_rho);
      p.y = m_rv->GetValue (-m_rho, m_rho);
      NS_LOG_LOGIC ("new random point: " << p << ", m_rho=" << m_rho);
    }
  while (!IsInsideCenteredNoRotation (p));

  // rotate and offset
  Vector p2;

  p2.x = p.x * cos (m_theta) - p.y * sin (m_theta);
  p2.y = p.x * sin (m_theta) + p.y * cos (m_theta);

  p2.x += m_x;
  p2.y += m_y;

  NS_LOG_DEBUG ("Hexagon position x=" << p2.x << ", y=" << p2.y);
  return p2;
}

int64_t
UniformHexagonPositionAllocator::AssignStreams (int64_t stream)
{
  m_rv->SetStream (stream);
  return 1;
}

bool
UniformHexagonPositionAllocator::IsInsideCenteredNoRotation (Vector q) const
{
  NS_LOG_FUNCTION (this << q);
  // method from http://www.playchilla.com/how-to-check-if-a-point-is-inside-a-hexagon

  // rotate to positive quadrant
  Vector q2 (std::abs (q.x), std::abs (q.y), 0);

  double v = m_rho / 2;
  double h = m_rho * cos (M_PI/6.0);

  // check bounding box
  if ((q2.x > h) || (q2.y > (2*v)))
    {
      return false;
    }

  // check dot product
  double dotProduct = (2*v*h - v*q2.x - h*q2.y);
  NS_LOG_LOGIC ("dot product = " << dotProduct);
  return (dotProduct >= 0);
}


} // namespace ns3
/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#ifndef MIN2D_DISTANCE_POSITION_ALLOCATOR_H
#define MIN2D_DISTANCE_POSITION_ALLOCATOR_H

#include "ns3/position-allocator.h"
#include "ns3/node-container.h"

namespace ns3 {

/**
 * This class will allocate positions using another position allocator and guaranteeing a minimum 2D distance from a provided set of points and/or nodes
 *
 */
class Min2dDistancePositionAllocator : public PositionAllocator
{
public:

  /**
   * Register this type with the TypeId system.
   * \return the object TypeId
   */
  static TypeId GetTypeId (void);
  Min2dDistancePositionAllocator ();
  virtual ~Min2dDistancePositionAllocator ();

  virtual Vector GetNext (void) const;
  virtual int64_t AssignStreams (int64_t stream);

  void SetPositionAllocator (Ptr<PositionAllocator> p);

  void AddNodesDistance (NodeContainer nodes, double distance);

  void AddPositionDistance (Vector position, double distance);

private:

  struct NodesDistance
  {
    NodeContainer nodes;
    double distance;
  };

  std::list<NodesDistance> m_nodesDistanceList;

  struct PositionDistance
  {
    Vector position;
    double distance;
  };

  std::list<PositionDistance> m_positionDistanceList;

  uint32_t m_maxAttempts;

  Ptr<PositionAllocator> m_positionAllocator;

};

} // namespace ns3

#endif /* MIN2D_DISTANCE_POSITION_ALLOCATOR_H */
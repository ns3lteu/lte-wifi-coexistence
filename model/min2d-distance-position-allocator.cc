/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2007 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mathieu Lacage <mathieu.lacage@sophia.inria.fr>
 */
#include "min2d-distance-position-allocator.h"
#include "ns3/mobility-model.h"
#include "ns3/double.h"
#include "ns3/uinteger.h"
#include "ns3/log.h"
#include <cmath>

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("Min2dDistancePositionAllocator");

NS_OBJECT_ENSURE_REGISTERED (Min2dDistancePositionAllocator);

TypeId
Min2dDistancePositionAllocator::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::Min2dDistancePositionAllocator")
    .SetParent<PositionAllocator> ()
    .SetGroupName ("Mobility")
    .AddConstructor<Min2dDistancePositionAllocator> ()
    .AddAttribute ("MaxAttempts",
                   "Maximum number of attempts to get a position satisfying the min 2D distance before giving up",
                   UintegerValue (1000),
                   MakeUintegerAccessor (&Min2dDistancePositionAllocator::m_maxAttempts),
                   MakeUintegerChecker<uint32_t> ())
  ;
  return tid;
}

Min2dDistancePositionAllocator::Min2dDistancePositionAllocator ()
{
}

Min2dDistancePositionAllocator::~Min2dDistancePositionAllocator ()
{
}

Vector
Min2dDistancePositionAllocator::GetNext (void) const
{
  bool satisfiesMin2dDistance;
  Vector p1;
  uint32_t attempts = 0;
  do
    {
      ++attempts;
      if (attempts > m_maxAttempts)
        {
          NS_FATAL_ERROR ("too many failed attempts, please revise your distance constraints");
        }

      satisfiesMin2dDistance = true;
      p1 = m_positionAllocator->GetNext ();
      Vector2D p12d (p1.x, p1.y);

      for (std::list<Min2dDistancePositionAllocator::NodesDistance>::const_iterator it
             = m_nodesDistanceList.begin ();
           satisfiesMin2dDistance && it != m_nodesDistanceList.end ();
           ++it)
        {
          for (NodeContainer::Iterator ncit = it->nodes.Begin ();
               satisfiesMin2dDistance && ncit != it->nodes.End ();
               ++ncit)
            {
              Vector p2 = (*ncit)->GetObject<MobilityModel> ()->GetPosition ();
              Vector2D p22d (p2.x, p2.y);
              double dist = CalculateDistance (p12d, p22d);
              satisfiesMin2dDistance &= (dist >= it->distance);
            }
        }

      for (std::list<Min2dDistancePositionAllocator::PositionDistance>::const_iterator it
             = m_positionDistanceList.begin ();
           satisfiesMin2dDistance && it != m_positionDistanceList.end ();
           ++it)
        {
          Vector2D p22d (it->position.x, it->position.y);
          double dist = CalculateDistance (p12d, p22d);
          satisfiesMin2dDistance &= (dist >= it->distance);
        }
    }
  while (!satisfiesMin2dDistance);
  return p1;
}

int64_t
Min2dDistancePositionAllocator::AssignStreams (int64_t stream)
{
  return m_positionAllocator->AssignStreams (stream);
}


void
Min2dDistancePositionAllocator::SetPositionAllocator (Ptr<PositionAllocator> p)
{
  m_positionAllocator = p;
}

void
Min2dDistancePositionAllocator::AddNodesDistance (NodeContainer nodes, double distance)
{
  Min2dDistancePositionAllocator::NodesDistance nd;
  nd.nodes = nodes;
  nd.distance = distance;
  m_nodesDistanceList.push_back (nd);
}

void
Min2dDistancePositionAllocator::AddPositionDistance (Vector position, double distance)
{
  Min2dDistancePositionAllocator::PositionDistance nd;
  nd.position = position;
  nd.distance = distance;
  m_positionDistanceList.push_back (nd);
}


} // namespace ns3
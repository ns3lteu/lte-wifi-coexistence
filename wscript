# -*- Mode: python; py-indent-offset: 4; indent-tabs-mode: nil; coding: utf-8; -*-

# def options(opt):
#     pass

# def configure(conf):
#     conf.check_nonfatal(header_name='stdint.h', define_name='HAVE_STDINT_H')

def build(bld):
    module = bld.create_ns3_module('lte-wifi-coexistence', ['lte','spectrum', 'wifi', 'internet-apps', 'flow-monitor', 'mobility', 'propagation'])
    module.source = [
        'helper/lte-wifi-coexistence-helper.cc',
        'helper/scenario-helper.cc',
        'helper/file-transfer-helper.cc',
        'model/basic-lbt-access-manager.cc',
        'model/duty-cycle-access-manager.cc',
        'model/lbt-access-manager.cc',
        'model/log-normal-shadowing-iid.cc',
        'model/los-nlos-classifier.cc',
        'model/ieee-80211ax-indoor-propagation-loss-model.cc',
        'model/itu-umi-propagation-loss-model.cc',
        'model/itu-inh-propagation-loss-model.cc',
        'model/min2d-distance-position-allocator.cc',
        'model/uniform-hexagon-position-allocator.cc',
        'model/file-transfer-application.cc',
        'model/voice-application.cc'
        ]

    module_test = bld.create_ns3_module_test_library('lte-wifi-coexistence')
    module_test.source = [
        'test/test-lte-unlicensed-interference.cc',
        'test/test-lte-interference-abs.cc',
        'test/test-lte-duty-cycle.cc',
        'test/lbt-access-manager-test.cc',
        'test/lbt-access-manager-ed-threshold-test.cc',
        'test/lbt-txop-test.cc',
        'test/voice-application-test-suite.cc'
        ]

    headers = bld(features='ns3header')
    headers.module = 'lte-wifi-coexistence'
    headers.source = [
        'helper/lte-wifi-coexistence-helper.h',
        'helper/scenario-helper.h',
        'helper/file-transfer-helper.h',
        'model/basic-lbt-access-manager.h',
        'model/duty-cycle-access-manager.h',
        'model/lbt-access-manager.h',
        'model/log-normal-shadowing-iid.h',
        'model/los-nlos-classifier.h',
        'model/ieee-80211ax-indoor-propagation-loss-model.h',
        'model/itu-umi-propagation-loss-model.h',
        'model/itu-inh-propagation-loss-model.h',
        'model/min2d-distance-position-allocator.h',
        'model/uniform-hexagon-position-allocator.h',
        'model/file-transfer-application.h',
        'model/voice-application.h',
        ]

    if bld.env.ENABLE_EXAMPLES:
        bld.recurse('examples')

    # bld.ns3_python_bindings()

